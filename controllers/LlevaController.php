<?php

namespace app\controllers;

use app\models\lleva;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * LlevaController implements the CRUD actions for lleva model.
 */
class LlevaController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all lleva models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => lleva::find(),
            /*
            'pagination' => [
                'pageSize' => 50
            ],
            'sort' => [
                'defaultOrder' => [
                    'numetapa' => SORT_DESC,
                    'código' => SORT_DESC,
                ]
            ],
            */
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single lleva model.
     * @param int $numetapa Numetapa
     * @param string $código Código
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($numetapa, $código)
    {
        return $this->render('view', [
            'model' => $this->findModel($numetapa, $código),
        ]);
    }

    /**
     * Creates a new lleva model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new lleva();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'numetapa' => $model->numetapa, 'código' => $model->código]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing lleva model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $numetapa Numetapa
     * @param string $código Código
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($numetapa, $código)
    {
        $model = $this->findModel($numetapa, $código);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'numetapa' => $model->numetapa, 'código' => $model->código]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing lleva model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $numetapa Numetapa
     * @param string $código Código
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($numetapa, $código)
    {
        $this->findModel($numetapa, $código)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the lleva model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $numetapa Numetapa
     * @param string $código Código
     * @return lleva the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($numetapa, $código)
    {
        if (($model = lleva::findOne(['numetapa' => $numetapa, 'código' => $código])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
