<?php

use yii\helpers\Html;

?>

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tabla de Apuestas</title>
    <style>
        body {
            margin: 0;
            display: flex;
        }

        #sidebar {
            width: 250px;
            height: 100vh;
            background-color: #ccc;
            position: fixed;
            top: 0;
            left: 0;
            display: flex;
            flex-direction: column;
            align-items: center;
            padding: 20px;
        }

        table {
            border-collapse: collapse;
            width: 70%;
            margin-left: 250px; /* Ajusta el ancho de la tabla según tus necesidades */
        }

        th, td {
            border: 1px solid #000;
            padding: 8px;
            text-align: left;
        }

        th {
            background-color: #000;
            color: #fff;
        }

        #apuesta-form, #confirmacion-form {
            display: flex;
            flex-direction: column;
            width: 100%;
            margin-top: 20px;
        }

        #confirmacion-form {
            display: none;
        }

        input, select, button {
            margin-bottom: 10px;
        }
         #apuesta-form h2 {
            margin-top: 20px; 
        }
         #confirmacion-form h2 {
            margin-top: 20px; 
        }
        
    </style>
</head>
<body>
    <div id="sidebar">
        <form id="apuesta-form">
            <h2>Haz tu apuesta</h2>
           <label for="ciclista">Ciclista:</label>
<select id="ciclista" name="ciclista">
    <?php foreach ($nombresCiclistas as $ciclista): ?>
        <option value="<?= $ciclista['nombre'] ?>"><?= $ciclista['nombre'] ?></option>
    <?php endforeach; ?>
</select>     
            <label for="categoria">Elige categoría:</label>
            <select id="categoria" name="categoria">
                <option value="etapas">Etapas</option>
                <option value="puertos">Puertos</option>
            </select>

            <label for="tu-apuesta">Tu apuesta:</label>
    <input type="number" id="tu-apuesta" name="tu-apuesta" placeholder="Ingresa tu apuesta" oninput="calcularValor()">
            <label for="multiplicador">Multiplicador:</label>
            <span id="multiplicador">0.000</span>
            
         
    <label for="tu-ganancia">Tu ganancia:</label>
    <span id="tu-ganancia">0.000</span>

            <button type="button" onclick="confirmarApuesta()">Aceptar</button>
        </form>

        <form id="confirmacion-form">
            <h2>Introduce tu correo electrónico para verificar la apuesta</h2>
            <label for="correo">Correo electrónico:</label>
            <input type="text" id="correo" name="correo" placeholder="example@tucorreo.com">
            <button type="button" onclick="apuestaConfirmada()">Aceptar</button>
        </form>
    </div>

    <table>
        <thead>
            <tr>
                <th>Ciclista</th>
                <th>Multiplicador para Etapas</th>
                <th>Multiplicador para Puertos</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($resultados as $resultado): ?>
                <tr>
                    <td><?= $resultado['nombre'] ?></td>
                    <td><?= number_format($multiplicadores[$resultado['nombre']]['etapas'], 3) ?></td>
                    <td><?= number_format($multiplicadores[$resultado['nombre']]['puertos'], 3) ?></td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>

    <script>
        function actualizarMultiplicador() {
        // Obtiene los valores seleccionados
        const ciclistaSeleccionado = ciclistaSelect.value;
        const categoriaSeleccionada = categoriaSelect.value;

        // Verifica si existen multiplicadores para el ciclista seleccionado
        if (multiplicadores[ciclistaSeleccionado] && multiplicadores[ciclistaSeleccionado][categoriaSeleccionada]) {
            multiplicadorSpan.innerText = multiplicadores[ciclistaSeleccionado][categoriaSeleccionada];
            calcularGanancia(); // Llama a la función de cálculo de ganancia cuando se actualiza el multiplicador
        } else {
            multiplicadorSpan.innerText = '0.000'; // Valor predeterminado si no hay coincidencia
        }
    }

    function calcularValor() {
        const tuApuesta = parseFloat(tuApuestaInput.value) || 0;
        const multiplicador = parseFloat(multiplicadorSpan.innerText) || 0;
        const tuGanancia = tuApuesta * multiplicador;

        // Actualiza el contenido del span con el resultado
        tuGananciaSpan.innerText = tuGanancia.toFixed(3);
    }

    // Nueva función para calcular la ganancia
    function calcularGanancia() {
        const tuApuesta = parseFloat(tuApuestaInput.value) || 0;
        const multiplicador = parseFloat(multiplicadorSpan.innerText) || 0;
        const tuGanancia = tuApuesta * multiplicador;

        // Actualiza el contenido del span con el resultado
        tuGananciaSpan.innerText = tuGanancia.toFixed(3);
    }
        function confirmarApuesta() {
            document.getElementById('apuesta-form').style.display = 'none';
            document.getElementById('confirmacion-form').style.display = 'flex';
        }

        function apuestaConfirmada() {
            alert("Apuesta realizada. Confirma en tu correo electrónico.");
            document.getElementById('apuesta-form').style.display = 'flex';
            document.getElementById('confirmacion-form').style.display = 'none';
        }
    </script>
</body>
</html>
