const ciclistaSelect = document.getElementById('ciclista');
const categoriaSelect = document.getElementById('categoria');
const multiplicadorSpan = document.getElementById('multiplicador');
const tuApuestaInput = document.getElementById('tu-apuesta');
const tuGananciaSpan = document.getElementById('tu-ganancia');


// Agrega un evento de cambio a los elementos select
ciclistaSelect.addEventListener('change', actualizarMultiplicador);
categoriaSelect.addEventListener('change', actualizarMultiplicador);
tuApuestaInput.addEventListener('input', calcularValor);

// Objeto que contiene los multiplicadores para cada ciclista y categoría
const multiplicadores = {
    'Alfonso Gutiérrez': {
        'etapas': 0.125,
        'puertos': 0.500
    },
   "Miguel Induráin": {
      "etapas": 0.200,
      "puertos": 0.500
   },
   "Giorgio Furlan": {
      "etapas": 0.286,
      "puertos": 0.600
   },
   "Davide Cassani": {
      "etapas": 0.286,
      "puertos": 0.600
   },
   "Per Pedersen": {
      "etapas": 0.333,
      "puertos": 0.600
   },
   "Jesus Montoya": {
      "etapas": 0.333,
      "puertos": 0.600
   },
   "Melchor Mauri": {
      "etapas": 0.333,
      "puertos": 0.75
   },
   "Mikel Zarrabeitia": {
      "etapas": 0.333,
      "puertos": 0.600
   },
   "Laurent Jalabert": {
      "etapas": 0.400,
      "puertos": 3.000
   },
   "Marco Saligari": {
      "etapas": 0.400,
      "puertos": 0.750
   },
   "Dimitri Konishev": {
      "etapas": 0.500,
      "puertos": 3.000
   },
   "Pedro Delgado": {
      "etapas": 0.500,
      "puertos": 3.000
   },
   "Alessio Di Basco": {
      "etapas": 0.667,
      "puertos": 3.000
   },
   "Gianni Bugno": {
      "etapas": 0.667,
      "puertos": 3.000
   },
   "Jean Van Poppel": {
      "etapas": 1.000,
      "puertos": 3.000
   },
   "Stefano della Santa": {
      "etapas": 1.000,
      "puertos": 3.000
   },
   "Eddy Seigneur": {
      "etapas": 1.000,
      "puertos": 3.000
   },
   "Armand de las Cuevas": {
      "etapas": 1.000,
      "puertos": 3.000
   },
   "Alex Zulle": {
      "etapas": 1.000,
      "puertos": 3.000
   },
   "Mario Cipollini": {
      "etapas": 2.000,
      "puertos": 3.000
   },
   "Tony Rominger": {
      "etapas": 2.000,
      "puertos": 3.000
   },
   "Claudio Chiappucci": {
      "etapas": 2.000,
      "puertos": 3.000
   },
   "Bruno Leali": {
      "etapas": 2.000,
      "puertos": 3.000
   },
   "Angel Yesid Camargo": {
      "etapas": 2.000,
      "puertos": 3.000
   },
   "Erwin Nijboer": {
      "etapas": 2.000,
      "puertos": 3.000
   },
   "Ivan Ivanov": {
      "etapas": 2.000,
      "puertos": 3.000
   },
   "Laurent Pillon": {
      "etapas": 2.000,
      "puertos": 3.000
   },
   "Pascal Lino": {
      "etapas": 2.000,
      "puertos": 3.000
   },
   "Stefano Zanini": {
      "etapas": 2.000,
      "puertos": 3.000
   },
   "Alberto Elli": {
      "etapas": 2.000,
      "puertos": 3.000
   },
   "Claudio Chioccioli": {
      "etapas": 2.000,
      "puertos": 3.000
   },
   "Francesco Casagrande": {
      "etapas": 2.000,
      "puertos": 3.000
   },
   "Jon Unzaga": {
      "etapas": 2.000,
      "puertos": 3.000
   },
   "Robert Millar": {
      "etapas": 2.000,
      "puertos": 3.000
   },
   "Vladislav Bobrik": {
      "etapas": 2.000,
      "puertos": 3.000
   },
   "Angel Citracca": {
      "etapas": 2.000,
      "puertos": 3.000
   },
   "Enrico Zaina": {
      "etapas": 2.000,
      "puertos": 3.000
   },
   "Giuseppe Petito": {
      "etapas": 2.000,
      "puertos": 3.000
   },
   "Laurent Fignon": {
      "etapas": 2.000,
      "puertos": 3.000
   },
   "Nestor Mora": {
      "etapas": 2.000,
      "puertos": 3.000
   },
   "Stefano Colage": {
      "etapas": 2.000,
      "puertos": 3.000
   },
   "Adriano Baffi": {
      "etapas": 2.000,
      "puertos": 3.000
   },
   "Casimiro Moreda": {
      "etapas": 2.000,
      "puertos": 3.000
   },
   "Fernando Mota": {
      "etapas": 2.000,
      "puertos": 3.000
   },
   "Mariano Picolli": {
      "etapas": 2.000,
      "puertos": 3.000
   },
   "prueba": {
      "etapas": 2.000,
      "puertos": 3.000
   },
   "Viatceslav Ekimov": {
      "etapas": 2.000,
      "puertos": 3.000
   },
   "Andrew Hampsten": {
      "etapas": 2.000,
      "puertos": 3.000
   },
   "Edgar Corredor": {
      "etapas": 2.000,
      "puertos": 3.000
   },
   "Lance Armstrong": {
      "etapas": 2.000,
      "puertos": 3.000
   },
   "Scott Sunderland": {
      "etapas": 2.000,
      "puertos": 3.000
   },
   "Bo Hamburger": {
      "etapas": 2.000,
      "puertos": 3.000
   },
   "Federico Garcia": {
      "etapas": 2.000,
      "puertos": 3.000
   },
   "Marco Giovannetti": {
      "etapas": 2.000,
      "puertos": 3.000
   },
   "Piotr Ugrumov": {
      "etapas": 2.000,
      "puertos": 3.000
   },
   "Gian Matteo Fagnini": {
      "etapas": 2.000,
      "puertos": 3.000
   },
   "Julian Gorospe": {
      "etapas": 2.000,
      "puertos": 3.000
   },
   "Rudy Verdonck": {
      "etapas": 2.000,
      "puertos": 3.000
   },
   "Antonio Esparza": {
      "etapas": 2.000,
      "puertos": 3.000
   },
   "Eugeni Berzin": {
      "etapas": 2.000,
      "puertos": 3.000
   },
   "Javier Mauleon": {
      "etapas": 2.000,
      "puertos": 3.000
   },
   "Luca Gelfi": {
      "etapas": 2.000,
      "puertos": 3.000
   },
   "Steffen Wesemann": {
      "etapas": 2.000,
      "puertos": 3.000
   },
   "Gerd Audehm": {
      "etapas": 2.000,
      "puertos": 3.000
   },
   "Juan Martinez Oliver": {
      "etapas": 2.000,
      "puertos": 3.000
   },
   "Massimiliano Lelli": {
      "etapas": 2.000,
      "puertos": 3.000
   },
   "Roberto Pagnin": {
      "etapas": 2.000,
      "puertos": 3.000
   },
   "Walte Castignola": {
      "etapas": 2.000,
      "puertos": 3.000
   },
   "Angel Edo": {
      "etapas": 2.000,
      "puertos": 3.000
   },
   "Erik Dekker": {
      "etapas": 2.000,
      "puertos": 3.000
   },
   "Hernan Buenahora": {
      "etapas": 2.000,
      "puertos": 3.000
   },
   "Nicola Minali": {
      "etapas": 2.000,
      "puertos": 3.000
   },
   "Agustin Sagasti": {
      "etapas": 2.000,
      "puertos": 3.000
   },
   "Flavio Giupponi": {
      "etapas": 2.000,
      "puertos": 3.000
   },
   "Johan Bruyneel": {
      "etapas": 2.000,
      "puertos": 3.000
   },
   "Marino Alonso": {
      "etapas": 2.000,
      "puertos": 3.000
   },
   "Raul Alcala": {
      "etapas": 2.000,
      "puertos": 3.000
   },
   "Vicente Aparicio": {
      "etapas": 2.000,
      "puertos": 3.000
   },
   "Angel Camarillo": {
      "etapas": 2.000,
      "puertos": 3.000
   },
   "Eleuterio Anguita": {
      "etapas": 2.000,
      "puertos": 3.000
   },
   "Giovanni Lombardi": {
      "etapas": 2.000,
      "puertos": 3.000
   },
   "Laurent Dufaux": {
      "etapas": 2.000,
      "puertos": 3.000
   },
   "Stefan Roche": {
      "etapas": 2.000,
      "puertos": 3.000
   },
   "Fernando Escartin": {
      "etapas": 2.000,
      "puertos": 3.000
   },
   "Jesper Skibby": {
      "etapas": 2.000,
      "puertos": 3.000
   },
   "Prudencio Induráin": {
      "etapas": 2.000,
      "puertos": 3.000
   },
   "Udo Bolts": {
      "etapas": 2.000,
      "puertos": 3.000
   },
   "Alvaro Mejia": {
      "etapas": 2.000,
      "puertos": 3.000
   },
   "Lale Cubino": {
      "etapas": 2.000,
      "puertos": 3.000
   },
   "Michele Bartoli": {
      "etapas": 2.000,
      "puertos": 3.000
   },
   "Sandro Heulot": {
      "etapas": 2.000,
      "puertos": 3.000
   },
   "Federico Echave": {
      "etapas": 2.000,
      "puertos": 3.000
   },
   "Javier Palacin": {
      "etapas": 2.000,
      "puertos": 3.000
   },
   "Manuel Guijarro": {
      "etapas": 2.000,
      "puertos": 3.000
   },
   "Tom Cordes": {
      "etapas": 2.000,
      "puertos": 3.000
   },
   "Dimitri Abdoujaparov": {
      "etapas": 2.000,
      "puertos": 3.000
   },
   "Gert-Jan Theunisse": {
      "etapas": 2.000,
      "puertos": 3.000
   },
   "Juan Romero": {
      "etapas": 2.000,
      "puertos": 3.000
   },
   "Massimo Podenzana": {
      "etapas": 2.000,
      "puertos": 3.000
   },
   "Rolf Aldag": {
      "etapas": 2.000,
      "puertos": 3.000
   },
   "William Palacios": {
      "etapas": 2.000,
      "puertos": 3.000
   }
    
};

